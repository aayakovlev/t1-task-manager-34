package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Update task by id.";

    @NotNull
    public static final String NAME = "task-update-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        System.out.print("Enter new name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter new description: ");
        @NotNull final String description = nextLine();

        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);

        getTaskEndpoint().updateById(request);
    }

}
