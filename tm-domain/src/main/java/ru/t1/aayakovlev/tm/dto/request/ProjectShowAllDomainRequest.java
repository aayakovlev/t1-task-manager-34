package ru.t1.aayakovlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectShowAllDomainRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectShowAllDomainRequest(@Nullable final String token) {
        super(token);
    }

}
