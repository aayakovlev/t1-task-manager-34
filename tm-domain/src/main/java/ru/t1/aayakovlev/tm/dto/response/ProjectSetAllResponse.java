package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectSetAllResponse extends AbstractProjectResponse {

    public ProjectSetAllResponse(@Nullable final List<Project> projects) {
        super(projects);
    }

}
