package ru.t1.aayakovlev.tm.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

public interface HasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull final Status status);

}
