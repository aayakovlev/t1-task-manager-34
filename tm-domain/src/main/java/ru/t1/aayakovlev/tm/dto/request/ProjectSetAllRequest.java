package ru.t1.aayakovlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectSetAllRequest extends AbstractUserRequest {

    @Nullable
    private List<Project> projects;

    public ProjectSetAllRequest(@Nullable final String token) {
        super(token);
    }

}
