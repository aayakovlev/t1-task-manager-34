package ru.t1.aayakovlev.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserShowAllRequest extends AbstractUserRequest {

    public UserShowAllRequest(@Nullable final String token) {
        super(token);
    }

}
