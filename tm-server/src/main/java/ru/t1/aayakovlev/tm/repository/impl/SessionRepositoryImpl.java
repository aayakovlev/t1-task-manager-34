package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.SessionRepository;

public final class SessionRepositoryImpl extends AbstractUserOwnedRepository<Session> implements SessionRepository {

}
