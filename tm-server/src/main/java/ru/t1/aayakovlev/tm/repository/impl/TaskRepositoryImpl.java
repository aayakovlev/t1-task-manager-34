package ru.t1.aayakovlev.tm.repository.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepositoryImpl extends AbstractUserOwnedRepository<Task> implements TaskRepository {

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name) {
        final Task task = new Task();
        task.setName(name);
        return save(userId, task);
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return save(userId, task);
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return models.stream()
                .filter((m) -> m.getProjectId() != null)
                .filter((m) -> projectId.equals(m.getProjectId()))
                .filter((m) -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
