package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Session;

public interface SessionRepository extends UserOwnedRepository<Session> {

}
